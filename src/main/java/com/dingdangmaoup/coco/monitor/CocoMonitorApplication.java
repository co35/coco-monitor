package com.dingdangmaoup.coco.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@EnableAdminServer
@SpringBootApplication
public class CocoMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CocoMonitorApplication.class, args);
    }

}
